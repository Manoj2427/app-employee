package com.imaginnovate.appemployee.common;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;

public class UtilityMethods {

    private static Double rateForMoreThanTwoAndHaflLac = 5.0;
    private static Double rateForMoreThanFivelLac = 10.0;
    private static Double rateForMoreThanTenLac = 20.0;

    private static Double cessRate=2.0;

    private static Double taxForMoreThanFiveLac = 12500d;
    private static Double taxForMoreThanSevenAndHalfLac = 25000d;

    private static int calculateTotalMonthsWorked(String doj) {
        // Define financial year period (April 1st to March 31st)
        LocalDate financialYearStart = LocalDate.parse("2024-04-01");
        LocalDate financialYearEnd = LocalDate.parse("2025-03-31");

        // Convert the joining month to LocalDate object
        LocalDate joiningDate = LocalDate.parse(doj);

        // Check if joining month is within the financial year
        if (joiningDate.isAfter(financialYearEnd)) {
            System.out.println("Employee joined outside the financial year");
            throw new IllegalArgumentException("Employee joined outside the financial year");
        }
        if (joiningDate.isBefore(financialYearStart)) {
            return 12;
        }

        // Calculate the number of months from the joining month to the end of the financial year
        long monthsWorked = ChronoUnit.MONTHS.between(joiningDate.withDayOfMonth(1), financialYearEnd.withDayOfMonth(1));

        return (int) monthsWorked;
    }

    public static Double getAnnualSalary(String doj, Double salary) {
        int totalMonth = calculateTotalMonthsWorked(doj);
        Double annualSalaryInMonths = salary * totalMonth;
        if (totalMonth != 12) {
            annualSalaryInMonths += calculateSalaryForDays(doj, salary);
        }
        return annualSalaryInMonths;
    }

    private static Double calculateSalaryForDays(String doj, Double salary) {
        LocalDate joiningDate = LocalDate.parse(doj);
        YearMonth yearMonth = YearMonth.of(joiningDate.getYear(), joiningDate.getMonth());
        double totalDaysInMonth = yearMonth.lengthOfMonth();
        double totalWorkedDays = (totalDaysInMonth - joiningDate.getDayOfMonth()) + 1;
        Double totalSalary = (((totalWorkedDays) / totalDaysInMonth) * salary);
        return totalSalary;
    }

    public static Double calculateTaxAmount(Double AnnualSalary) {
        Double tax = 0d;
        if (AnnualSalary <= 250000) {
            System.out.println("No Tax");
        } else if (AnnualSalary > 250000 && AnnualSalary <= 500000) {
            Double TaxableIncome = AnnualSalary - 250000d;
             tax = getTaxWithRate(TaxableIncome, rateForMoreThanTwoAndHaflLac);
        } else if (AnnualSalary > 500000 && AnnualSalary <= 1000000) {
            Double TaxableIncome = AnnualSalary - 500000d;
            tax = taxForMoreThanFiveLac + getTaxWithRate(TaxableIncome, rateForMoreThanFivelLac);

        } else if (AnnualSalary > 1000000) {
            Double TaxableIncome = AnnualSalary - 1000000d;
             tax = taxForMoreThanFiveLac + taxForMoreThanSevenAndHalfLac + getTaxWithRate(TaxableIncome, rateForMoreThanTenLac);
        }
        return tax;
    }

    private static Double getTaxWithRate(Double salary, Double rate) {
        return (salary * (rate / 100));
    }
    public static Double calculateCessAmount(Double AnnualSalary) {
        Double cessAmount=0d;
        if(AnnualSalary>2500000){
            Double applicableAmount = AnnualSalary-2500000d;
            cessAmount = getTaxWithRate(applicableAmount,cessRate);
        }
        return cessAmount;
    }
}
