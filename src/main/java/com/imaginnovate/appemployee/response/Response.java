package com.imaginnovate.appemployee.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Response {
    String message;
    Integer statusCode;
}
