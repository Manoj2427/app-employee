package com.imaginnovate.appemployee.services.impl;

import com.imaginnovate.appemployee.common.UtilityMethods;
import com.imaginnovate.appemployee.dao.EmployeeDao;
import com.imaginnovate.appemployee.dto.EmployeeDto;
import com.imaginnovate.appemployee.globalexception.UserNotFound;
import com.imaginnovate.appemployee.mapper.EmployeeMapper;
import com.imaginnovate.appemployee.repository.EmployeeRepo;
import com.imaginnovate.appemployee.response.Response;
import com.imaginnovate.appemployee.services.EmployeeService;
import lombok.SneakyThrows;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    EmployeeRepo employeeRepo;

    private final EmployeeMapper mapper = Mappers.getMapper(EmployeeMapper.class);

    @Override
    public Response createEmployee(EmployeeDto employeeDto) {
        Response response= new Response();
        EmployeeDao employeeDao = mapper.EmployeeDtoToEmployeeDao(employeeDto);
        EmployeeDao savedEmployeeDao = employeeRepo.save(employeeDao);
        EmployeeDto savedEmployeeDto = mapper.EmployeeDaoToEmployeeDto(savedEmployeeDao);
        if(savedEmployeeDto.getId()>0){
            response.setStatusCode(HttpStatus.CREATED.value());
            response.setMessage("Successfully Employee Saved with ID : "+savedEmployeeDto.getId());
        }else {
            response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            response.setMessage("Employee not Saved...Please try again!!");
        }

        return response;
    }

    @Override
    public Map<String, Object> getTaxDetails(Integer id) {
        Optional<EmployeeDao> employeeDao=employeeRepo.findById(id);
        Map<String,Object> details= new HashMap<>();
        if(employeeDao.isPresent()){
            EmployeeDto employeeDto=  mapper.EmployeeDaoToEmployeeDto(employeeDao.get());
            details.put("id : ", employeeDto.getId());
            details.put("First Name :", employeeDto.getFirstName());
            details.put("Last Name :", employeeDto.getLastName());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String doj= sdf.format(employeeDto.getDateOfJoining());
            Double AnnualSalaryOfEmployee = UtilityMethods.getAnnualSalary(doj,employeeDto.getSalary());
            details.put("Annual Salary", AnnualSalaryOfEmployee);
            details.put("Tax Amount", UtilityMethods.calculateTaxAmount(AnnualSalaryOfEmployee));
            details.put("Cess Amount", UtilityMethods.calculateCessAmount(AnnualSalaryOfEmployee));
        }else{
                throw new UserNotFound("User not found with id: " + id);
        }
        return details;
    }
}
