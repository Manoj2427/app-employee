package com.imaginnovate.appemployee.dao;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.annotation.Nonnull;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringJoiner;

@Entity
@Table(name = "employee")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "firstname")
    @Nonnull
    private String firstName;

    @Column(name = "lastname")
    @Nonnull
    private String lastName;

    @Column(name = "email",unique = true)
    @Nonnull
    private String email;

    @Column(name = "phonenumber")
    @Nonnull
    private String phonenumber;

    @Column(name = "doj")
    @Nonnull
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date dateOfJoining;

    @Column(name = "salary")
    @Nonnull
    private Double salary;

    public void setPhonenumber(List<String> phoneNumbers) {
        StringJoiner joiner = new StringJoiner(",");
        for (String phoneNumber : phoneNumbers) {
            joiner.add(phoneNumber);
        }
        this.phonenumber = joiner.toString();
    }

    public List<String> getPhonenumber() {
        List<String> listOfNumber=new ArrayList<>();
        String[] eachPhone=phonenumber.split(",");
        for(String phone:eachPhone){
            listOfNumber.add(phone);
        }
        return listOfNumber;
    }
}
