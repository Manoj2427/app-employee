package com.imaginnovate.appemployee.services;

import com.imaginnovate.appemployee.dto.EmployeeDto;
import com.imaginnovate.appemployee.globalexception.UserNotFound;
import com.imaginnovate.appemployee.response.Response;


import java.util.Map;

public interface EmployeeService {

    Response createEmployee(EmployeeDto employeeDto);
    Map<String,Object> getTaxDetails(Integer id) throws UserNotFound;
}
