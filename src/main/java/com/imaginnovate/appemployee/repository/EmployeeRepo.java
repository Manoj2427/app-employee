package com.imaginnovate.appemployee.repository;

import com.imaginnovate.appemployee.dao.EmployeeDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepo extends JpaRepository<EmployeeDao,Integer> {
}
