package com.imaginnovate.appemployee.controller;

import com.imaginnovate.appemployee.dto.EmployeeDto;
import com.imaginnovate.appemployee.response.Response;
import com.imaginnovate.appemployee.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @PostMapping("/employee")
    public ResponseEntity<Response> createEmployee(@RequestBody EmployeeDto employeeDto) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(employeeService.createEmployee(employeeDto));
    }

    @GetMapping("/employee/{id}")
    public ResponseEntity<Map<String, Object>> getTaxDeatils(@PathVariable Integer id) {

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(employeeService.getTaxDetails(id));
    }

}
