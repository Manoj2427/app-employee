package com.imaginnovate.appemployee.globalexception;

public class UserNotFound extends RuntimeException{
    public UserNotFound(String msg){
        super(msg);
    }
}
