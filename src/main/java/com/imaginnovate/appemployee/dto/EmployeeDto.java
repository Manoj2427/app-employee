package com.imaginnovate.appemployee.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.ComponentScan;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDto {

    @JsonIgnore
    private Integer id;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("firstName")
    private String firstName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("lastName")
    private String lastName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("email")
    private String email;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("phonenumber")
    private List<String> phonenumber;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("dateOfJoining")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date dateOfJoining;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("salary")
    private Double salary;

    public void setEmail(String email) {
        if (isValidEmail(email)) {
            this.email = email;
        } else {
            throw new IllegalArgumentException("Invalid email format");
        }
    }
    private boolean isValidEmail(String email) {
        // Implement validation logic
        // This is a basic example, you can use a regex or a library like javax.mail.internet.InternetAddress
        return email != null && email.matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$");
    }

    public void setPhonenumber(List<String> phonenumber) {
        if (isValidMobileNumber(phonenumber)) {
            this.phonenumber = phonenumber;
        } else {
            throw new IllegalArgumentException("Invalid mobile number format");
        }
    }

    private boolean isValidMobileNumber(List<String> mobileNumbers) {
        // Implement validation logic
        // This is a basic example, you can use a regex or a library like libphonenumber
        for(String mobileNumber : mobileNumbers){
            if(mobileNumber == null || (!mobileNumber.matches("^\\d{10}$"))){
                return false;
            }
        }
        return true;
    }

    public void setDateOfJoining(Date dateOfJoining) {
        this.dateOfJoining = dateOfJoining;
    }
}
