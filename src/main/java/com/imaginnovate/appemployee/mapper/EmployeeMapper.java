package com.imaginnovate.appemployee.mapper;

import ch.qos.logback.core.model.ComponentModel;
import com.imaginnovate.appemployee.dao.EmployeeDao;
import com.imaginnovate.appemployee.dto.EmployeeDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.context.annotation.ComponentScan;

@Mapper(componentModel = "spring")
public interface EmployeeMapper {

    //@Mapping(target = "firstName", source = "firstName")
//    @Mapping(source = "lastName", target = "lastName")
//    @Mapping(source = "email", target = "email")
//    @Mapping(source = "phonenumber", target = "phonenumber")
//    @Mapping(source = "dateOfJoining", target = "dateOfJoining")
//    @Mapping(source = "salary", target = "salary")
    //@Mapping(target = "id", ignore = true)
    EmployeeDto EmployeeDaoToEmployeeDto(EmployeeDao employeeDao);

//    @Mapping(source = "firstName", target = "firstName")
//    @Mapping(source = "lastName", target = "lastName")
//    @Mapping(source = "email", target = "email")
//    @Mapping(source = "phonenumber", target = "phonenumber")
//    @Mapping(source = "dateOfJoining", target = "dateOfJoining")
//    @Mapping(source = "salary", target = "salary")
    EmployeeDao EmployeeDtoToEmployeeDao(EmployeeDto employeeDto);

}
